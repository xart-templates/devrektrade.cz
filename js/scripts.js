jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	// page
	var page_height = function(){
		var height = $(window).height();
		$('.page').css({'min-height':height-100});
	}
	page_height();
	$(window).scroll(function(){
		page_height();
	});





	// mainmenu - active menu items when scrolling window and click menu item
	// Cache selectors
	var lastId,
		topMenu = $('.mod_menu-main'),
		menuItems = topMenu.find('a'),
		scrollItems = menuItems.map(function(){
			var item = $($(this).attr('href'));
			if (item.length) { return item; }
	});
	// Bind click handler to menu items
	$('a[href*="#"]').click(function(e){
		var href = $(this).attr('href');
		var offsetTop = href === '#' ? 0 : $(href).offset().top;
		$('html, body').stop().animate({ 
			scrollTop: offsetTop
		}, 600);
		e.preventDefault();
	});
	// Bind to scroll
	$(window).scroll(function(){
		var fromTop = $(this).scrollTop();
		var cur = scrollItems.map(function(){
			if ($(this).offset().top < fromTop+10)
			return this;
		});
		cur = cur[cur.length-1];
		var id = cur && cur.length ? cur[0].id : '';
		if (lastId !== id) {
			lastId = id;
			menuItems
			.parent().removeClass('act')
			.end().filter('[href=#'+id+']').parent().addClass('act');
		}                   
	});





	// go top
	var go_top = function(){
		var offset = $(window).scrollTop();
		if(offset>100){
			$('#gotop').addClass('on');
		} else {
			$('#gotop').removeClass('on');
		}
	}
	go_top();
	$(window).scroll(function(){
		go_top();
	});
	$('#gotop a').click(function(){ 
		$('html,body').animate({scrollTop:0},600);
		return false;
	});

});